//!
//! [Day 15: Beacon Exclusion Zone](https://adventofcode.com/2022/day/15)
//!
//! This one should be run with optimizations (more than a second without unoptimized)
//!

use std::collections::HashSet;

struct Puzzle {
    /// Sensors and their distance to the closest beacon
    sensors: Vec<(i64, i64, i64)>,
    /// Set of all the beacons
    beacons: HashSet<(i64, i64)>,
    /// Row in which to count spots without beacons
    row: i64,
    /// Size of the maximum square to check for the beacon
    max: i64,
}

impl Puzzle {
    fn new() -> Self {
        Self {
            sensors: Vec::new(),
            beacons: HashSet::new(),
            row: 0,
            max: 0,
        }
    }

    fn row_occupation(&self, row: i64, max: Option<i64>) -> Vec<(i64, i64)> {
        let mut segments = Vec::new();

        // Intersect sensed regions and beacon with the targeted row
        for (sensor_col, sensor_row, to_beacon) in &self.sensors {
            let to_row = (sensor_row - row).abs();
            if to_row > *to_beacon {
                continue;
            }
            let start = sensor_col - (to_beacon - to_row);
            let end = sensor_col + (to_beacon - to_row);
            if let Some(value) = max {
                if start == start.clamp(0, value - 1) || end == end.clamp(0, value - 1) {
                    segments.push((start.clamp(0, value - 1), end.clamp(0, value - 1)));
                }
            } else {
                segments.push((start, end));
            }
        }

        // Merge segments. Easier when sorted
        segments.sort_unstable();
        let mut segments_merged = Vec::<(i64, i64)>::new();
        for segment in &segments {
            if let Some(current) = segments_merged.last_mut() {
                if segment.0 > current.1 {
                    segments_merged.push(*segment);
                } else {
                    current.1 = current.1.max(segment.1);
                }
            } else {
                segments_merged.push(*segment);
            }
        }

        segments_merged
    }

    fn configure(&mut self, path: &str) {
        let mut data = std::fs::read_to_string(path).unwrap();
        data.pop();

        for line in data.split('\n') {
            let mut splitted = line.split(&['=', ',', ':']);
            let s_x = splitted.nth(1).unwrap().parse::<i64>().unwrap();
            let s_y = splitted.nth(1).unwrap().parse::<i64>().unwrap();
            let b_x = splitted.nth(1).unwrap().parse::<i64>().unwrap();
            let b_y = splitted.nth(1).unwrap().parse::<i64>().unwrap();
            self.beacons.insert((b_x, b_y));
            let dist_x = (s_x - b_x).abs();
            let dist_y = (s_y - b_y).abs();
            self.sensors.push((s_x, s_y, dist_x + dist_y));
        }
    }

    fn part1(&self) -> i64 {
        let segments = self.row_occupation(self.row, None);
        let occupation: i64 = segments.iter().map(|(a, b)| *b - *a + 1).sum();
        let beacon_count = self.beacons.iter().filter(|(_, y)| *y == self.row).count();
        occupation - i64::try_from(beacon_count).unwrap()
    }

    fn part2(&self) -> i64 {
        let mut beacon_position = (0, 0);
        for row in 0..self.max {
            let segments = self.row_occupation(row, Some(self.max));
            let occupation: i64 = segments.iter().map(|(a, b)| *b - *a + 1).sum();
            if occupation + 1 == self.max {
                beacon_position.0 = segments.first().unwrap().1 + 1;
                beacon_position.1 = row;
                break;
            }
        }
        beacon_position.0 * 4_000_000 + beacon_position.1
    }
}

/// Test from puzzle input
#[test]
fn test01() {
    let mut puzzle = Puzzle::new();
    puzzle.configure("test01.txt");
    puzzle.row = 10;
    puzzle.max = 20;
    assert_eq!(puzzle.part1(), 26);
    assert_eq!(puzzle.part2(), 56000011);
}

/// Test from user input
#[test]
fn test02() {
    let mut puzzle = Puzzle::new();
    puzzle.configure("test02.txt");
    puzzle.row = 2_000_000;
    puzzle.max = 4_000_000;
    assert_eq!(puzzle.part1(), 5_838_453);
    assert_eq!(puzzle.part2(), 12_413_999_391_794);
}

fn main() {
    let mut puzzle = Puzzle::new();
    let input = std::env::args().nth(1).expect("No input file");
    puzzle.configure(&input);
    puzzle.row = 2_000_000;
    puzzle.max = 4_000_000;
    println!("{}", puzzle.part1());
    println!("{}", puzzle.part2());
}
